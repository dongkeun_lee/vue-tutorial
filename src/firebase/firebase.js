// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
import * as firebase from "firebase/app";

// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/firestore";

var firebaseConfig = {
  apiKey: "AIzaSyCkR-xVJEd5ytl3ty9VCNuZw0RPekUcAgw",
  authDomain: "self-study-dafc6.firebaseapp.com",
  databaseURL: "https://self-study-dafc6.firebaseio.com",
  projectId: "self-study-dafc6",
  storageBucket: "self-study-dafc6.appspot.com",
  messagingSenderId: "968979986463", 
  appID: "1:968979986463:web:65c1c0a3eb83f453007d2a",
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
