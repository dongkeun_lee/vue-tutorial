import Vue from 'vue'
import Login from '@/components/Login'

describe('Login.vue', () => {
  it('should render correct contents', () => {
    expect(typeof Login.data).toBe('function')
    const defaultData = Login.data()
    expect(defaultData.email).toBe('')
  })
})
